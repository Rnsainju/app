from rest.viewset import *
from rest_framework import routers

router=routers.DefaultRouter()
router.register('user',UserViewset)
router.register('devicedetails',DeviceDetailsViewset)
router.register('company',CompanyViewset)
router.register('notice',NoticeViewset)
router.register('notification',NotificationViewset)
