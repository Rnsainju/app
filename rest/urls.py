from django.conf.urls import include,url
from django.urls import re_path
from .import views
from django.urls import path

app_name='rest'


urlpatterns=[
    re_path(r'^validate_phone/',views.ValidateOTP.as_view()),
    re_path("^auth/validate_otp/$",views.ValidateOTP.as_view()),
    url(r'^api-auth/',include('rest_framework.urls'))
]

