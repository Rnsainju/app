from rest_framework import viewsets
from . import models
from . import serializers

class UserViewset(viewsets.ModelViewSet):
    queryset=models.User.objects.all()
    serializer_class=serializers.UserSerializer

class DeviceDetailsViewset(viewsets.ModelViewSet):
    queryset=models.DeviceDetails.objects.all()
    serializer_class=serializers.DeviceDetailsSerializer

class NoticeViewset(viewsets.ModelViewSet):
    queryset=models.Notice.objects.all()
    serializer_class=serializers.NoticeSerializer

class CompanyViewset(viewsets.ModelViewSet):
    queryset=models.Company.objects.all()
    serializer_class=serializers.AppSerializer

class NotificationViewset(viewsets.ModelViewSet):
    queryset=models.Notifications.objects.all()
    serializer_class=serializers.NotificationSerializer