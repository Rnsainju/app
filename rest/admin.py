from django.contrib import admin
from .models import Company,Notifications, Notice, DeviceDetails


admin.site.register(Company)
admin.site.register(Notice)
admin.site.register(Notifications)
admin.site.register(DeviceDetails)

