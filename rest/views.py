from django.shortcuts import render
from django.contrib.auth.models import User
from rest_framework import viewsets
from rest_framework.response import Response
from django.shortcuts import get_object_or_404
from rest_framework import generics
from rest_framework.views import APIView
from .models import Company, Notifications, DeviceDetails, Notice, PhoneOtp

class ValidateOTP(APIView):
    """ 
    If you have received otp, post a request with phone and that otp will be redirected to set the password
    """

    def post(self, request, *args, **kwargs):
        phone=request.data.get('phone',False)
        otp_sent=request.data.get('otp',False)

        if phone and otp_sent:
            old=PhoneOtp.objects.filter(phone_iexact =phone)
            if old.exists():
                old=old.first()
                otp=old.otp
                if str(otp_sent)== str(otp):
                    old.validated=True
                    old.save()
                    return Response({
                        'status':True,
                        'detail':'OTP Matched'
                    })
                else:
                    return Response({
                        'status':False,
                        'detail':'OTP INCORRECT'
                    })
            else:
                 return Response({
                        'status':False,
                        'detail':'First proceed via sending otp request'
                    })
        else:
             return Response({
                        'status':False,
                        'detail':'Please provide both phone and otp for validation'
                    })