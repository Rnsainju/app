from django.db import models
from django.contrib.auth.models import User
import random
import os
from django.core.validators import RegexValidator

def get_filename_ext(filepath):
    base_name = os.path.basename(filepath)
    name, ext = os.path.splitext(base_name)
    return name, ext


def upload_image_path(instance, filename):
    new_filename = random.randint(1, 3910209312)
    name, ext = get_filename_ext(filename)
    final_filename = '{new_filename}{ext}'.format(
        new_filename=new_filename, ext=ext)
    return "business/{final_filename}".format(
        new_filename=new_filename,
        final_filename=final_filename
    )


class DeviceDetails(models.Model):
    STATUS_CHOICES = (
        ('active', 'Active'),
        ('in-active', 'In-active'),
    )
    phone_type=models.CharField(max_length=30)
    location=models.CharField(max_length=50, blank=True, null=True)
    added=models.DateTimeField(auto_now_add=True)
    last_login=models.DateTimeField(auto_now_add=True)
    is_active=models.CharField(max_length=20, choices=STATUS_CHOICES)
    
    def __str__(self):
        return self.phone_type

class Notice(models.Model):
    name=models.CharField(max_length=20, blank=True,null=True)
    file=models.FileField(upload_to=upload_image_path, null=True, blank=True)
    uploaded_at=models.DateTimeField(auto_now_add=True)
    
    def __str__(self):
        return self.name

class Notifications(models.Model):
    name=models.CharField(max_length=10, blank=True, null=True)
    text=models.TextField()
    created_at=models.DateTimeField(auto_now_add=True)
    
    def __str__(self):
        return self.name

class Company(models.Model):
    name = models.CharField(max_length=50, unique=True)
    contact_information = models.CharField(
        max_length=50, unique=True, null=True)
    location = models.CharField(max_length=50, blank=True, null=True)
    short_description = models.TextField()
    description = models.TextField()
    notice=models.name = models.ForeignKey('Notice',  on_delete=models.SET_NULL, blank=True, null=True)
    otp=models.CharField(max_length=4, unique=True)
    notification=models.ManyToManyField(Notifications)

    def __str__(self):
        return self.name

class PhoneOtp(models.Model):
    phone_regex=RegexValidator( regex =r'^\+?\d(9,14)$', message="Phone number must be entered in the format: '+97798********'. Up to 14 digits")
    phone= models.CharField(validators=[phone_regex], max_length=17, unique=True)
    otp=models.CharField(max_length=9, blank=True, null=True)
    count=models.IntegerField(default=0, help_text='Number of otp sent')
    validated=models.BooleanField(default=False,help_text='If true, user validate otp correctly in second api')

    def __str__(self):
        return str(self.phone) + 'is sent' + str(self.otp)
