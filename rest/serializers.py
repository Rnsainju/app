from rest_framework import serializers
from .models import Company, Notifications, DeviceDetails, Notice
from django.contrib.auth.models import User

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model=User
        fields='__all__'

class DeviceDetailsSerializer(serializers.ModelSerializer):
    class Meta:
        model=DeviceDetails
        fields='__all__'

class AppSerializer(serializers.ModelSerializer):
    class Meta:
        model=Company
        fields='__all__'

class NoticeSerializer(serializers.ModelSerializer):
    class Meta:
        model=Notice
        fields='__all__'

class NotificationSerializer(serializers.ModelSerializer):
    class Meta:
        model=Notifications
        fields='__all__'